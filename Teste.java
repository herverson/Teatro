import javax.swing.JOptionPane;

public class Teste
{

		public static void main(String[] args)
		{
			int andares = 0;
			int linhas = 0;
			int colunas = 0;
			int vip = 0;
			float precoCad;
			int i;
			int totEspetaculos = Integer.parseInt(JOptionPane.showInputDialog("Informe o total de espetaculos"));
			Espetaculo[] espetaculo = new Espetaculo[totEspetaculos];

			andares = Integer.parseInt(JOptionPane.showInputDialog("Informe o número de andares"));
			linhas = Integer.parseInt(JOptionPane.showInputDialog("Informe o número de linhas"));
			colunas = Integer.parseInt(JOptionPane.showInputDialog("Informe o número de colunas"));

			for (i = 0; i < espetaculo.length; i++)
			{
				JOptionPane.showMessageDialog(null, "Espetaculo" + (i+1));
				// criar o teatro

				vip = Integer.parseInt(JOptionPane.showInputDialog("Informe o andar vip"));
				precoCad = Float.parseFloat(JOptionPane.showInputDialog("Informe o preço da cadeira vip"));

				// receber os dados do espetaculo
				String nomeEspetaculo = JOptionPane.showInputDialog("Informe o nome do espetaculo");
				float preco = Float.parseFloat(JOptionPane.showInputDialog("Informe o preco de espetaculo"));
				String data = JOptionPane.showInputDialog("Informe a data do espetaculo");
				String endereco = JOptionPane.showInputDialog("Informe o endereço do espetaculo");
				String horario = JOptionPane.showInputDialog("Informe o horário do espetaculo");
				int censura = Integer.parseInt(JOptionPane.showInputDialog("Informe a censura"));

				// criar os objetos espetaculo
				espetaculo[i] = new Espetaculo(nomeEspetaculo, preco, data, endereco, horario, censura);

			}
			// menu
			String espetaculos = "1 -" + espetaculo[0].getNome();

			for (i = 1; i < espetaculo.length; i++)
			{
				espetaculos += " \n"+ (i + 1) + " -" + espetaculo[i].getNome();
			}
			int escolha;

			int op = Integer.parseInt(JOptionPane.showInputDialog("Qual espetaculo você quer comprar ?\n" + espetaculos));
			boolean debito = Boolean.parseBoolean(JOptionPane.showInputDialog("Débito"));
			float dinheiro = Float.parseFloat(JOptionPane.showInputDialog("Dinheiro do cliente"));
			Ingresso v1 = new Ingresso(espetaculo[op-1], debito, dinheiro, andares, linhas, colunas);
			v1.imprimir();

			int andar = Integer.parseInt(JOptionPane.showInputDialog("Informe o andar"));
			int linha = Integer.parseInt(JOptionPane.showInputDialog("Informe a linha"));
			int coluna = Integer.parseInt(JOptionPane.showInputDialog("Informe a coluna"));

			v1.compra(andar-1, linha-1, coluna-1);
			escolha = Integer.parseInt(JOptionPane.showInputDialog("Deseja continuar comprando? 1-sim 0-não"));
			while (escolha == 1)
			{
				op = Integer.parseInt(JOptionPane.showInputDialog("Qual espetaculo você quer comprar ?\n" + espetaculos));
				debito = Boolean.parseBoolean(JOptionPane.showInputDialog("Débito"));
				if (!debito)
					dinheiro = Float.parseFloat(JOptionPane.showInputDialog("Dinheiro do cliente"));
				v1.setEspetaculo(espetaculo[op-1]);
				v1.setDebito(debito);
				v1.setDinheiro(dinheiro);
				v1.imprimir();
				// compra ingresso
				andar = Integer.parseInt(JOptionPane.showInputDialog("Informe o andar"));
				linha = Integer.parseInt(JOptionPane.showInputDialog("Informe a linha"));
				coluna = Integer.parseInt(JOptionPane.showInputDialog("Informe a coluna"));

				v1.compra(andar-1, linha-1, coluna-1);
				 escolha = Integer.parseInt(JOptionPane.showInputDialog("Deseja continuar comprando? 1-sim 0-não"));
				//v1.imprimir();
			}


			for (i = 0; i < espetaculo.length; i++)
			{
				System.out.println("O total de arrecadação do espetaculo " + espetaculo[i].getNome()
						+ " foi de " + espetaculo[i].getArrecadacao());
			}
		}
	}
