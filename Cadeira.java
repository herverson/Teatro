public class Cadeira
{
	private String cor;
	private int num;	

	public int getNum()
	{
		return num;
	}

	public void setNum(int num)
	{
		this.num = num;
	}

	public String getCor()
	{
		return cor;
	}

	public void setCor(String cor)
	{
		this.cor = cor;
	}

	@Override
	public String toString()
	{
		return "Cadeira cor=" + cor;
	}
}
