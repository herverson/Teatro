public class Ingresso
{
	private Espetaculo espetaculo;
	private Cadeira[][][] cadeira;
	private CadeiraVip[][][] cadeiraVip;
	private boolean debito;
	private float dinheiro;
	private int andares;
	private int linhas;
	private int colunas;


	public Ingresso() {}

	Ingresso(Espetaculo espetaculo, boolean debito, float dinheiro, int andares, int linhas, int colunas)
	{
		this.espetaculo = espetaculo;
		this.debito = debito;
		this.dinheiro = dinheiro;
		this.andares = andares;
		this.linhas = linhas;
		this.colunas = colunas;
		this.cadeira = new Cadeira[andares][linhas][colunas];
		for (int i = 0; i < andares; i++)
		{
			for (int j = 0; j < linhas; j++)
			{
				for (int k = 0; k < colunas; k++)
				{
					this.cadeira[i][j][k] = new Cadeira();
				}
			}
		}
	}

	public boolean preenchida(int andar, int linha, int coluna)
	{
		if (this.cadeira[andar][linha][coluna].getNum() == 0)
			return false;
		else
			return true;
	}

	public void compra(int andar, int linha, int coluna)
	{

		if (preenchida(andar, linha, coluna))
			System.out.println("Cadeira preenchida");
		else
		{
			System.out.println(toString());
			this.cadeira[andar][linha][coluna].setNum(1);
			espetaculo.setArrecadacao(espetaculo.getPreco());
		}

	}

	public void imprimir()
	{
		for (int i = 0; i < andares; i++)
		{
			for (int j = 0; j < linhas; j++)
			{
				for (int k = 0; k < colunas; k++)
				{
					if (cadeira[i][j][k].getNum() == 0)
						System.out.println("andar "+ (i + 1) + " linha " + (j + 1) + " coluna " + (k + 1));
					else
						System.out.println("X andar "+ (i + 1) + " linha " + (j + 1) + " coluna " + (k + 1));
				}
			}
			System.out.println();
		}
	}

	public Espetaculo getEspetaculo()
	{
		return espetaculo;
	}


	public void setEspetaculo(Espetaculo espetaculo)
	{
		this.espetaculo = espetaculo;
	}


	public Cadeira[][][] getCadeira()
	{
		return cadeira;
	}


	public void setCadeira(Cadeira[][][] cadeira)
	{
		this.cadeira = cadeira;
	}


	public boolean isDebito()
	{
		return debito;
	}


	public void setDebito(boolean debito)
	{
		this.debito = debito;
	}


	public float getDinheiro()
	{
		return dinheiro;
	}


	public void setDinheiro(float dinheiro)
	{
		this.dinheiro = dinheiro;
	}

	public int getAndares()
	{
		return andares;
	}

	public void setAndares(int andares)
	{
		this.andares = andares;
	}

	public int getLinhas()
	{
		return linhas;
	}

	public void setLinhas(int linhas)
	{
		this.linhas = linhas;
	}

	public int getColunas()
	{
		return colunas;
	}

	public void setColunas(int colunas)
	{
		this.colunas = colunas;
	}

	@Override
	public String toString()
	{
		return "Ingresso \n" + espetaculo +
				"\ncadeira: " + (andares+1) + (linhas+1) + (colunas+1) + "\nTroco: " + (getDinheiro() - espetaculo.getPreco());
	}

}
