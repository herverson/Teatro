public class Espetaculo
{
	private String nome;
	private float preco;
	private String data;
	private String endereco;
	private String horario;
	private int censura;
	private float arrecadacao;

	public Espetaculo(String nome, float preco, String data, String endereco, String horario, int censura)
	{
		this.nome = nome;
		this.preco = preco;
		this.data = data;
		this.endereco = endereco;
		this.horario = horario;
		this.censura = censura;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public float getPreco()
	{
		return preco;
	}

	public void setPreco(float preco)
	{
		this.preco = preco;
	}

	public String getData()
	{
		return data;
	}

	public void setData(String data)
	{
		this.data = data;
	}

	public String getEndereco()
	{
		return endereco;
	}

	public void setEndereco(String endereco)
	{
		this.endereco = endereco;
	}

	public String getHorario()
	{
		return horario;
	}

	public void setHorario(String horario)
	{
		this.horario = horario;
	}

	public int getCensura()
	{
		return censura;
	}

	public void setCensura(int censura)

	{
		this.censura = censura;
	}

	public float getArrecadacao()
	{
		return arrecadacao;
	}

	public void setArrecadacao(float arrecadacao)
	{
		this.arrecadacao += arrecadacao;
	}

	@Override
	public String toString()
	{
		return "Espetaculo nome: " + nome + "\npreço: " + preco + "\nendereço :" + endereco + "\ndata: " + data
				+ "\nhorario: " + horario + "\ncensura: " + censura;
	}


}
